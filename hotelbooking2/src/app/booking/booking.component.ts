import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import './date.ext';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { Router } from '@angular/router';


@Component({
    templateUrl: 'booking.component.html',
    styleUrls: ['./booking.component.scss']
})

export class BookingComponent {
    constructor(private http: HttpClient, private router: Router) { }
    private bookingUrl = "http://83.233.104.137:3000/";
    // private bookingUrl: string = "https://6034c2c9843b1500179333b7.mockapi.io/users"; //mockserver for testing
    private fetchedData: Object = {}
    TOTALROOMS: number = 2;
    minDate: Date = new Date();
    maxDate: Date = new Date().addDays(365);
    startDate: Date | null = null;
    endDate: Date | null = null;

    invalidForm: boolean = false;
    unsuccessfulPost: boolean = false;

    private unavailabledays = new Array<string>();
    private roomsperday = new Map<string, Array<number>>();

    private roomNo: number = 1;

    ngOnInit() {
        this.http.get(this.bookingUrl).subscribe((response: Object) => {
            this.fetchedData = Object(response).data;
            this.roomsperday = this.roomPerDay(this.fetchedData)
            this.checkUnavailableDays();
            console.log(this.fetchedData);
            console.log(this.roomsperday);
        })
    }

    /*filterUnavailableDays
    Takes the list of all unavailable days (all rooms booked)
    and send it to the calender to remove them from the display
    */
    filterUnavailableDays = (d: Date | null): boolean => {
        if (!d) return false;
        return !this.unavailabledays.includes(d?.toLocaleDateString());
    }

    /*onStartDateChange
    Happens if the user changes the date-range in the calendar, it makes
    sure that all dates that are unavailable changes accordingly
    */
    onStartDateChange(type: string, event: MatDatepickerInputEvent<Date>) {
        let datePicked = event.value;
        if (!datePicked) return;

        //Set new maxDate to the first unavailable date after startDate is picked.
        this.maxDate = this.findFirstUnavailableDay(datePicked);


        //Filters and sorts(dates in ascending order) all dates less than picked date. 
        let sortedData = Array.from(this.roomsperday).filter(value => {
            if (!datePicked) return;
            return new Date(Object(value[0])).getTime() - datePicked.getTime() > 0;
        }).sort((a: any, b: any) => {
            return +new Date(a[0]) - +new Date(b[0]);
        })

        let firstUnavailableDayPerRoom = new Map<number, string>();
        //for each room, find first unavailable date and set to map.
        for (let i = 1; i <= this.TOTALROOMS; i++) {
            //find first occurence of roomNo. 
            let firstDay = sortedData.find((value, index) => {
                return value[1].includes(i);
            });
            //if firstDay exists, add to map.
            if (firstDay) {
                firstUnavailableDayPerRoom.set(i, firstDay[0]);
            }
        }
        //if mapsize = totalrooms, we need to find new maxDate since its not possible to find room without changing.
        if (firstUnavailableDayPerRoom.size >= this.TOTALROOMS) {
            let tempMaxDate: string = datePicked?.toLocaleDateString();
            firstUnavailableDayPerRoom.forEach(startDate => {
                if (new Date(startDate).getTime() > new Date(tempMaxDate).getTime()) {
                    tempMaxDate = startDate;
                }
            })
            this.maxDate = new Date(tempMaxDate).addDays(-1); // because maxDate is exclusive, we dont want them to be able to pick the maxDate.
        }

    }
    onEndDateChange(type: string, event: MatDatepickerInputEvent<Date>) {
        /**
         * find best room for date range.
         */
        let datePicked = event.value;
        if (!datePicked) return;

        //Save all rooms that are booked within startDate and endDate
        let bookedRooms = new Set<number>();

        this.roomsperday.forEach((value, key) => {
            if (!this.startDate) return;
            if (!this.endDate) return;
            if (!datePicked) return;

            if (new Date(key) >= new Date(this.startDate.toLocaleDateString()) && new Date(key) <= new Date(datePicked.toLocaleDateString())) {
                for (let roomNo of value) {
                    bookedRooms.add(roomNo);
                }
            }
        })
        bookedRooms = new Set(Array.from(bookedRooms).sort()); // sort the set
        //we know at least one room has to be available, find it!
        let tmpRoom = 1;
        for (let i = 1; i <= this.TOTALROOMS; i++) {
            if (bookedRooms.has(i)) {
                tmpRoom++;
            } else {
                this.roomNo = tmpRoom;
                return;
            }
        }

    }

    clearDate() {
        this.startDate = null;
        this.endDate = null;
        this.minDate = new Date();
        this.maxDate = new Date().addDays(365);
    }

    /*roomPerDay:
    saves a map that contains unavailable rooms (saves date as a key
    and the booked rooms in an array as value)*/
    roomPerDay(data: Object) {
        let rpd = new Map<string, Array<number>>();
        Object(data).forEach((book: Object) => {
            let room = Object(book).roomNo;
            let dateRange = this.dateRange(new Date(Object(book).startDate), new Date(Object(book).endDate));
            dateRange.forEach((date: string) => {
                if (!rpd.has(date)) {
                    rpd.set(date, [room]);
                } else {
                    let roomList = rpd.get(date);
                    if (roomList) {
                        roomList.push(room);
                        rpd.set(date, roomList);
                    }
                }
            })
        })
        return rpd;
    }

    /*findFirstUnavailableDay
    If someone chooses a day with a later date where all rooms are booked
    => shouldn't be able to chose a range past that date. This functions
    stops that from happening
    */
    findFirstUnavailableDay(d: Date | null): Date {
        let maxDate = new Date().addDays(365);
        if (this.unavailabledays.length === 0 || d === null) {
            return maxDate;
        } else {
            for (let date of this.unavailabledays) {
                if (d < new Date(date)) {
                    maxDate = new Date(date);
                    return maxDate
                }
            }
            return maxDate;
        }
    }

    /*dateRange
    Returns an array of all dates between start- and end date.
    */
    dateRange(startDate: Date, endDate: Date): Array<string> {
        let dateRangeArray = new Array<string>();
        let first = startDate;
        while (first <= endDate) {
            dateRangeArray.push(first.toLocaleDateString())
            first = first.addDays(1);
        }
        return dateRangeArray;
    }

    /*checkUnavailableDays
    Check if a day has all rooms unavailable => save in a list
    */
    checkUnavailableDays() {
        this.roomsperday.forEach((rooms: Array<number>, date: string) => {
            if (rooms.length >= this.TOTALROOMS) {
                this.unavailabledays.push(date);
            }
        })
    }

    onSubmit(data: any) {
        if (data.form.status === "VALID") {
            this.invalidForm = false;
            let email = data.value.email;
            let startDate = new Date(data.value.startDate).toLocaleDateString();
            let endDate = new Date(data.value.endDate).toLocaleDateString();
            let body = {
                "email": email,
                "roomNo": this.roomNo,
                "startDate": startDate,
                "endDate": endDate
            };
            console.log(body);
            this.http.post(this.bookingUrl, body).subscribe((response: any) => {
                if (response.success) {
                    this.unsuccessfulPost = false;
                    this.router.navigate([`/my_bookings/${response.bookingCode}`]);
                } else {
                    this.unsuccessfulPost = true;
                }
            });
            this.clearDate();
        } else {
            this.invalidForm = true;
        }
    }
}
