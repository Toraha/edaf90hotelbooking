import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home';
import { BookingComponent } from './booking';
import { AboutComponent } from './about';
import { ContactComponent } from './contact';
import { MyBookingsComponent } from './my_bookings';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'booking', component: BookingComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'about', component: AboutComponent },
  { path: 'my_bookings', component: MyBookingsComponent },
  { path: 'my_bookings/:caller', component: MyBookingsComponent },

  { path: '**', redirectTo: '' } // otherwise redirect to home
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
