import {Component} from '@angular/core';
import { Params, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({ 
  templateUrl: 'contact.component.html',
  styleUrls: ['./contact.component.scss']
})

export class ContactComponent {
 submitted = false; 
 valid: boolean = true;
 success: boolean = true;

 constructor(private http: HttpClient, private router: Router) { }
 private contactUrl = "http://83.233.104.137:3000/contact";
 
 ngOnInit(){
   console.log("yes")
 }
 
 onSubmit(data: any) {
  if (data.form.status === "VALID") {
      this.valid = true;
      let name = data.value.text;
      let email = data.value.email;
      let subject = data.value.subject;
      let message = data.value.message;
      let body = {
          "name" : name,
          "email": email,
          "subject": subject,
          "message": message
      };
      console.log(body);
      this.http.post(this.contactUrl, body).subscribe((response: any) => {
          if (response.success) {
              this.success = true;

          } else {
              this.success = false;
          }
      });
  } else {
      this.valid = false;
  }
}
}
