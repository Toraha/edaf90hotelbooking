import { Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({ 
  templateUrl: 'my_bookings.component.html',
  styleUrls: ['./my_bookings.component.scss']
 })

export class MyBookingsComponent { 
  constructor (private route: ActivatedRoute) {}

  code = '';
  private bookingsURL = "http://83.233.104.137:3000/";
  bookingCodeInvalid = false;   // Keeps track if the user has entered a valid booking ID
  found = false;                // Keeps track if a booking's been found
  cancelled = false;            // Keeps track if a booking's been cancelled
  roomNbr = 0;
  startDate = "";
  endDate = "";

  bookingCode: string | undefined;

  ngOnInit(){
    this.route.params.subscribe((params: Params) => this.bookingCode = params['caller']);

    if (this.bookingCode) {
      this.code = this.bookingCode;
      this.getBooking();
    }
  }

  // Search for booking if ENTER is pressed and there's new input
  onEnter(code: string) { 
    if (code != '') {
      this.code = code;
      this.getBooking();
    }
  }

  // Fetches booking based on booking ID
  getBooking() {
    this.cancelled = false;

    fetch(this.bookingsURL + this.code)
    .then(res => res.json())
    .then(responseObject => {
      
      // Found, save booking details
      if (responseObject.success) {
        this.roomNbr = responseObject.data.roomNo;
        this.startDate = responseObject.data.startDate;
        this.endDate = responseObject.data.endDate;
        this.bookingCodeInvalid = false;
        this.found = true;

      } else {

        // Not found
        this.bookingCodeInvalid = true;
        this.found = false;
        console.log("Something went wrong... :/", responseObject.message)
      }
    });
  }

  // Deletes booking from database
  removeBooking() {
    fetch((this.bookingsURL + this.code),{method:"DELETE"})
    .then(res => res.json())
    .then(responseObject => {

      // Found, delete it
      if (responseObject.success){
        this.cancelled = true;
        this.resetValues();
    } else {

      // Not found
        console.error("Something went wrong... :/", responseObject.message)
        throw responseObject
    }
    });
  }

  // Resets attributes after booking is removed
  resetValues() {
    this.found = false;
    this.bookingCodeInvalid = false;
    this.roomNbr = 0;
    this.startDate = "";
    this.endDate = "";
  }

}
