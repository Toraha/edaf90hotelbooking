import { Component, OnInit } from '@angular/core';
//import { AppModule } from '././app.module';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  slides = [{'image': 'https://images.unsplash.com/photo-1582719508461-905c673771fd?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1225&q=80'}, // beach view
  {'image': 'https://47ulw640o9fpqq4la1v9y43r-wpengine.netdna-ssl.com/wp-content/uploads/2015/09/iStock-995624510-3000x1500.jpg'}, // hotel lobby
  {'image': 'https://images.unsplash.com/photo-1563889859803-7c2160099b24?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80'}, // breakfast
  {'image': 'https://cdn.pixabay.com/photo/2019/01/07/13/55/buffet-3919191_960_720.jpg'}, // lunch
  {'image': 'https://images.unsplash.com/photo-1583338917496-7ea264c374ce?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80'}, // dinner
  {'image': 'https://images.unsplash.com/photo-1550966871-3ed3cdb5ed0c?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1050&q=80'}, // restaurant
  {'image': 'https://techcrunch.com/wp-content/uploads/2016/12/dream-presidential-suite-terrace.jpg'}, // pool room
  {'image': 'https://markhotel-production.s3.amazonaws.com/app/uploads/2015/04/manhattan-suite-living-and-bedroom-1-2000x1333.jpg'}, // suite
  {'image': 'https://images.unsplash.com/photo-1540558870477-e8c59bf88421?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80'},  // gym
  {'image': 'https://images.unsplash.com/photo-1600334129128-685c5582fd35?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80'}, // spa
  {'image': 'https://images.unsplash.com/photo-1543539571-2d88da875d21?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80'} // drink by the pool
]; 
  constructor() { }

  ngOnInit(): void {
  }

}
