const db = require("./db")
const uniqueString = require('unique-string');

module.exports = {
  set,
  print
}

const databaseColumns = ['contact_id','name', 'email', 'subject', 'message']

function print(callback){ 
  const sql = "SELECT * FROM messages";
  db.query(sql,callback);
}

function set({name, email, subject, message}, callback){  
  const sql = "INSERT INTO messages(name, email, subject, message) VALUES (?, ?, ?, ?)"
  db.query(sql,[name, email, subject, message],(err,res)=>callback(err,res));   
}
