const express = require('express');
const router = express.Router();
const service = require('./bookingService')

router.get('/favicon.ico', favicon)
router.get('/print', print)
router.get('/:id', getByBookingCode);
router.put('/:id', editBooking);
router.delete('/:id', _delete);
router.post('/', book);
router.get('/', get);


function formatDate(date){
  const dd = String(date.getDate()).padStart(2, '0');
  const mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
  const yyyy = date.getFullYear();
  return `${yyyy}-${mm}-${dd}`;
}

function parseBody(body){
  if(Object.values(body)[0] === "") return {success:false, message:"Can't read body data. Is Content-Type: application/json in header?"}
  const startDate = new Date(body.startDate);
  const endDate = new Date(body.endDate);
  if(!(startDate instanceof Date) || isNaN(startDate) || !(endDate instanceof Date) || isNaN(endDate)){
   return {success:false, message:"Did not understand one or both of inputed dates. startDate:"+body.startDate+", endDate:"+body.endDate};
  }
  const roomNo = body.roomNo;
  if(!/^\d+$/.test(roomNo)){
   return {success:false, message:"Error with room number. roomNo:"+body.roomNo};
  }
  const email = body.email;
  if(!/\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b/gi.test(email)){
   return {success:false, message:"Email is not correct. email:"+body.email};
  }
  return {success:true, body:{startDate:formatDate(startDate), endDate:formatDate(endDate), roomNo, email}};
}

function querize(query){
  return {
    sortOrder:query.sortOrder || "ASC",
    sortField:query.sortField || "id",
    from:query.from,
    to:query.to
  };
}
function print(res, res){
  service.print((error, result)=>{
    if(error) res.json({...error, success:false});
    else res.json({success:true, data:result});
  });
}

function get(req, res){
  const params = querize(req.query);
  params.email = req.query.email;
  params.roomNo = req.query.roomNo;
  service.get(params,(error, result)=>{
    if(error) res.json({...error, success:false});
    else res.json({success:true, data:result});
  });
}

function getByBookingCode(req, res){
  if(req.params.id)
    service.get({bookingCode:req.params.id},(error, result)=>{
      if(error) res.json({...error, success:false});
      else if(result.length) res.json({data:result[0], success:true});
      else res.json({success:false, message:"No booking found with bookingCode:"+req.params.id});
    });
  else res.json({success:false, message:"Error with bookingCode whilst get booking by bookingcode (pass it as <api>/:id). bookingCode:"+req.params.id})
}

function _delete(req, res){
  service.remove(req.params.id,(error, result)=>{
    if(error) res.json({...error, success:false});
    else res.json({success:true});
  });
}

function editBooking(req, res){
  const {body, success, message} = parseBody(req.body);
  if(success){
    if(req.params.id)
      service.set({bookingCode:req.params.id, ...body},(error, result)=>{
        if(error) res.json({...error, success:false});
        else res.json({success:true});
      });
    else res.json({success:false, message:"Error with bookingCode whilst edit booking (pass it as <api>/:id). bookingCode:"+req.params.id})
  }else res.json({success:false, message:message});
}

function book(req, res){
  const {body, success, message} = parseBody(req.body);
  if(success){
    service.set(body,(error, result)=>{
      if(error) res.json({...error, success:false});
      else res.json({success:true, bookingCode:result});
    });
  }else res.json({success:false, message:message});
}

function favicon(req, res){
  res.send("\"favicon\"")
}

module.exports = router;
