const db = require("./db")
const uniqueString = require('unique-string');

module.exports = {
  get,
  set,
  remove,
  print
}

const DAYSINFUTURE = 14; // default time when no date is inputed
const databaseColumns = ['id', 'email', 'startDate', 'endDate', 'bookingCode', 'roomNo']

function print(callback){
  db.query("SELECT id, email, bookingCode, roomNo, DATE_FORMAT(startDate, '%Y-%m-%d') as startDate, DATE_FORMAT(endDate, '%Y-%m-%d') as endDate FROM bookings",callback)
}

function get({bookingCode, roomNo, email, sortOrder, sortField, from, to}, callback){
  const filter = bookingCode?["bookingCode="+db.escape(bookingCode)]:["startDate BETWEEN "+(from?db.escape(from):"CURDATE()")+" AND "+(to?db.escape(to):"CURDATE() + INTERVAL "+DAYSINFUTURE+" DAY")]
  if(roomNo) filter.push("roomNo="+db.escape(roomNo))
  if(email) filter.push("email="+db.escape(email))  
  const sql = "SELECT roomNo, DATE_FORMAT(startDate, '%Y-%m-%d') as startDate, DATE_FORMAT(endDate, '%Y-%m-%d') as endDate FROM bookings WHERE "+filter.join(" AND ")+ " ORDER BY "+(databaseColumns.includes(sortField)?db.escapeId(sortField):'id')+" "+((sortOrder == "ASC")?"ASC":"DESC");
  db.query(sql,callback)
}

function set({bookingCode, roomNo, email, startDate, endDate}, callback){  
  bookingCode = bookingCode || uniqueString()
  const sql = "INSERT INTO bookings(bookingCode, roomNo, email, startDate, endDate) VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE roomNo=?, email=?, startDate=?, endDate=?"
  db.query(sql,[bookingCode, roomNo, email, startDate, endDate, roomNo, email, startDate, endDate],(err,res)=>callback(err,bookingCode));   
}

function remove(bookingCode, callback){
  db.query("DELETE FROM bookings WHERE bookingCode=?", [bookingCode], callback)
}