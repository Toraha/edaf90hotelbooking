# Backend of hotelbooking

## working (hopefully) api @ http://83.233.104.137:3000/

booking input query examples. Leave empty for default values:
```
"startDate=2021-05-10&endDate=2021-05-20&roomNo=2&email=a.b@c.de&sortField=startDate&sortOrder=ASC"
```
```
{"startDate":"2021-05-10","endDate":"2021-05-20","roomNo":2,"email":"a.b@c.de","sortField":"startDate","sortOrder":"ASC"}
```
Note: set header: "Content-Type: application/json" if using that format

## Usage:
* debug print: `GET <api>/print` Gets the entire database. Just for debugging purpose
* get all: `GET <api>/?<query>` Gets all bookings with filter specified in query. Default query (if nothing is set): `startDate=<today>&endDate=<two weeks ahead>`
* get one: `GET <api>/<bookingCode>` Gets the booking specified by the bookingCode.
* Book: `POST <api>/` Creates a booking with booking data in request body in any format specified in input example.
* edit booking: `PUT <api>/<bookingCode>` Edit the booking with specified bookingCode with data in request body in any format specified in input example.
* delete booking: DELETE `<api>/<bookingCode>` Delets the booking with specified bookingCode.

## Returns
Instead of extending the error codes,  All requests returns an json object with `{success:<true/false>}` depending on if the request succeeded.
If the request did not succeed information is passed along with information about what went wrong, often with a "message" for human readability. `{success:false, message:"<message>", ...OtherInformation}`
If the get request succeeded a data object is passed along with the expected data. `{success:true, data:[{startDate, endDate, roomNo},{startDate, endDate, roomNo},...]`  or `{success:true, data:{startDate, endDate, roomNo}}`
If the post request succeeded the bookingCode is also returned `{success:true, bookingCode:"<bookingCode>"}`

## example usage:
### print
```javascript
fetch('<api>/print')
.then(res => res.json())
.then(responseObject =>{
  if(responseObject.success){
    console.log("Yippee! :D")
    console.table(responseObject.data) //<-- array with the entire database
  }else{
    console.error("Something went wrong... :/", responseObject.message)
    throw responseObject
  }
});
```


### get all
```javascript
fetch('<api>/')
.then(res => res.json())
.then(responseObject =>{
  if(responseObject.success){
    console.log("Yippee! :D")
    console.table(responseObject.data) //<-- array of bookings
  }else{
    console.error("Something went wrong... :/", responseObject.message)
    throw responseObject
  }
});
```

### get one
```javascript
fetch('<api>/<bookingCode>')
.then(res => res.json())
.then(responseObject =>{
  if(responseObject.success){
    console.log("Yippee! :D")
    console.log(responseObject.data) //a booking
  }else{
    console.error("Something went wrong... :/", responseObject.message)
    throw responseObject
  }
});
```

### book
```javascript
fetch('<api>',{
  method:"POST",
  "Content-Type": "application/json"
  body: JSON.stringify({"startDate":"2021-05-10","endDate":"2021-05-20","roomNo":2,"email":"a.b@c.de","sortField":"startDate","sortOrder":"ASC"})
})
.then(res => res.json())
.then(responseObject =>{
  if(responseObject.success){
    console.log("Yippee! :D")
    console.log(responseObject.bookingCode) //a bookingCode
  }else{
    console.error("Something went wrong... :/", responseObject.message)
    throw responseObject
  }
});
```

### edit booking
```javascript
fetch('<api>/<bookingCode>',{
  method:"PUT",
  "Content-Type": "application/json"
  body: JSON.stringify({"startDate":"2021-05-10","endDate":"2021-05-20","roomNo":2,"email":"a.b@c.de","sortField":"startDate","sortOrder":"ASC"})
})
.then(res => res.json())
.then(responseObject =>{
  if(responseObject.success){
    console.log("Yippee! :D")
  }else{
    console.error("Something went wrong... :/", responseObject.message)
    throw responseObject
  }
});
```

### delete booking
```javascript
fetch('<api>/<bookingCode>',{method:"PUT"})
.then(res => res.json())
.then(responseObject =>{
  if(responseObject.success){
    console.log("Yippee! :D")
  }else{
    console.error("Something went wrong... :/", responseObject.message)
    throw responseObject
  }
});
```
