const express = require('express');
const router = express.Router();
const service = require('./contactService')

router.post('/', putContact);
router.get('/getContact', getContact);


function parseBody(body){
  if(Object.values(body)[0] === "") return {success:false, message:"Can't read body data. Is Content-Type: application/json in header?"}
  const name = body.name;
  const subject= body.subject;
  const message = body.message;

  const email = body.email;
  if(!/\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b/gi.test(email)){
   return {success:false, message:"Email is not correct. email:"+body.email};
  }
  return {success:true, body:{name, email, subject, message}};
}

function getContact(req, res){
  service.print((error, result)=>{
    if(error) res.json({...error, success:false});
    else res.json({success:true, data:result});
  });
}

function putContact(req, res){
  const {body, success, message} = parseBody(req.body);
  if(success){
    service.set(body,(error, result)=>{
      if(error) res.json({...error, success:false});
      else res.json({success:true});
    });
  }else res.json({success:false, message:message});
}


module.exports = router;
