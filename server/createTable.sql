CREATE TABLE bookings(
   id INT AUTO_INCREMENT PRIMARY KEY,
   email VARCHAR(255) NOT NULL,
   startDate DATE NOT NULL,
   endDate DATE NOT NULL,
   roomNo INT NOT NULL,
   bookingCode VARCHAR(32) UNIQUE
);

CREATE TABLE messages(
  contact_id INT AUTO_INCREMENT,
  name TEXT NOT NULL,
  email VARCHAR(255) NOT NULL,
  subject TEXT,
  message TEXT,
  PRIMARY KEY (contact_id)
 );
