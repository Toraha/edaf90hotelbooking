const express = require('express');
const app = express();
const cors = require('cors');

app.use(cors());
app.use(express.urlencoded({extended:false}));
app.use(express.json());
app.use('/contact', require('./contactController'));
app.use('/', require('./bookingController'));


const port = 3000;
const server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});
